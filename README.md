# React-dnd-dz-test

參與專案建置需依照專案規格實作指定功能，
實作過程務必注意 code 可閱讀性及可維護性，
盡量以best practices 為實作依據進行開發，
並加入適當註解以備 code review。

過程中，若有開發困難或需求變動，歡迎連絡討論。

## 實作目標

以React + Typescript 開發一項多功能儀表板

### 遵守條件
- (必須)採用 redux-toolkit [(官方文件)](https://redux-toolkit.js.org/)
- (必須)採用 dnd-kit [(官方文件)](https://github.com/clauderic/dnd-kit) 進行拖曳功能開發，可參考官方example功能
- (可選)採用樣式框架 bootstrap、 tailwind-css、material-ui 等協助開發
- 以上條件若有更好的解決方案，歡迎討論後更動

### 須達成目標
- 中央儀表板區域可容納多個卡片
- 卡片間可以拖曳變換順序
- 可觸發喚出/收合側邊選單
    - 側邊選單有兩種以上不同卡片版型
    - 卡片版型可拖曳至中央儀表板區域變為卡片

### 加分項目(非必須完成)
- 卡片可自訂選擇顯示樣式(兩種版型，Line chart 或 純數值 等...)
- 卡片可自訂選擇 mock 資料來源 (氣象局OpendataAPI或其他資料來源皆可)
- 卡片可以拖曳入另一個模塊中，成為子層結構，最多共兩層即可
- 卡片再儀表板中能夠自由拖曳並放置
- 卡片具備 resize 功能
